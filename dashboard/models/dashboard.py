from odoo import models, fields, api
import datetime


class Dashboard(models.Model):
    _name = 'dashboard.dashboard'
    

    def _compute_field_list(self):
        dashboard=self.env['dashboard.settings'].search([],limit=1,order='id desc')
        lists = dashboard.line_ids
        last_slices_list=[]
        for list in lists:
            if list.display:
                if list.type=='money':  
                    requete="""SELECT sum("""+list.field_id.name+""") as field FROM """+list.model_id.model.replace('.','_')
                else:
                    requete="""SELECT count("""+list.field_id.name+""") as field FROM """+list.model_id.model.replace('.','_')
                if list.filter:
                    requete+=""" Where """+list.filter
                self.env.cr.execute(requete.replace("''",""))
                result = self.env.cr.dictfetchall()[0]
                field=result['field']
                last_slices_list.append([field,list.name or list.field_id.field_description,list.color,list.icon])
        return last_slices_list
            
    
    def _get_default_chart(self):
        chart_list=[]
        for list in self.env['dashboard.settings'].search([],limit=1,order='id desc').chart_ids:
            if list.display:
                chart_list.append([list.id,list.name])
                
        return chart_list
    
    def _get_default_chartbar(self):
        chart_list=[]
        for list in self.env['dashboard.settings'].search([],limit=1,order='id desc').bar_chart_ids:
            if list.display:
                chart_list.append([list.id,list.name])
        return chart_list
    
    name = fields.Char('Name')
    company_id = fields.Many2one('res.company', string='Company', required=True, readonly=True,
                                 default=lambda self: self.env.user.company_id)
    currency_id = fields.Many2one('res.currency', related='company_id.currency_id', store=True, string="Currency")
    field_list = fields.Selection('_compute_field_list',string='Slices names')
    chart_list=fields.Selection('_get_default_chart',string='Charts')
    bar_chart_ids=fields.Selection('_get_default_chartbar',string='Bar Charts')
    
    @api.multi
    def action_setting(self):
        
        action = self.env.ref('dashboard.action_dashboard_config').read()[0]

        setting = self.env['dashboard.settings'].search([],limit=1,order='id desc').id
        action['views'] = [(self.env.ref('dashboard.dashboard_config_settings').id, 'form')]
        action['res_id'] = setting
        return action
    