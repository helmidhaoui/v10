# -*- coding: utf-8 -*-
from odoo import http
from odoo.http import request,Controller, route
import json

class Dashboard(http.Controller):
    
    
    @route('/page/dashboard', type='http', auth='public')
    def get_uid(self):
        cr = request.cr
        res=[]
        dashboard=request.env['dashboard.settings'].search([],limit=1,order='id desc')
        for chart in dashboard.chart_ids:
            if chart.display:
                if chart.type=='money': 
                    requete="""SELECT sum("""+chart.chart_measure_field_id.name+""") as sale, date("""+chart.chart_date_field_id.name+""") as date 
                        FROM """+chart.chart_model_id.model.replace('.','_')
                else:
                    requete="""SELECT count("""+chart.chart_measure_field_id.name+""") as sale, date("""+chart.chart_date_field_id.name+""") as date 
                        FROM """+chart.chart_model_id.model.replace('.','_')
                if chart.filter:
                    requete+=""" Where """+chart.filter
                    
                requete+="""
                    Group by date("""+chart.chart_date_field_id.name+""")
                    order by date("""+chart.chart_date_field_id.name+""")"""
                
                cr.execute(requete)
                result = cr.dictfetchall()
                
                res.append([chart.id,result])
        return http.request.make_response(json.dumps(res,{
            'Cache-Control': 'no-cache', 
            'Content-Type': 'JSON; charset=utf-8',
            'Access-Control-Allow-Origin':  '*',
            'Access-Control-Allow-Methods': 'GET',
            'Access-Control-Allow-Headers': 'Content-Type, Access-Control-Allow-Headers, X-Requested-With',

            })) 
        
    @route('/page/dashboardbar', type='http', auth='public')
    def get_bar(self):
        cr = request.cr
        res=[]
        dashboard=request.env['dashboard.settings'].search([],limit=1,order='id desc')
        for chart in dashboard.bar_chart_ids:
            if chart.display:
                if chart.type=='money': 
                    requete="""SELECT sum("""+chart.chart_ykey_id.name+""") as mesure, date("""+chart.chart_xkey_id.name+""") as date 
                        FROM """+chart.chart_model_id.model.replace('.','_')
                else:
                    requete="""SELECT count("""+chart.chart_ykey_id.name+""") as mesure, date("""+chart.chart_xkey_id.name+""") as date 
                        FROM """+chart.chart_model_id.model.replace('.','_')
                if chart.filter:
                    requete+=""" Where """+chart.filter
                    
                requete+="""
                    Group by date("""+chart.chart_xkey_id.name+""")
                    order by date("""+chart.chart_xkey_id.name+""")"""
                
                cr.execute(requete)
                result = cr.dictfetchall()
                res.append([chart.id,result])
        return http.request.make_response(json.dumps(res,{
            'Cache-Control': 'no-cache', 
            'Content-Type': 'JSON; charset=utf-8',
            'Access-Control-Allow-Origin':  '*',
            'Access-Control-Allow-Methods': 'GET',
            'Access-Control-Allow-Headers': 'Content-Type, Access-Control-Allow-Headers, X-Requested-With',

            })) 
        
        
